﻿using System;
using System.Collections.Generic;
using CocosSharp;
using Tetris.Common;
using System.Reflection;
using System.Linq;

namespace Tetris
{
	public class GameLayer : CCLayerColor
	{
		TetrisBlock block;
		Type[] blockTypes;
		TetrisGrid grid;
		CCLabel scoreValueLabel;
        CCLabel levelValueLabel;
        CCLabel linesValueLabel;
        int score = 0;
        int level = 0;
        int lines = 0;
        float gravity = 0.8f;
		bool messageShown = false;
		CCDrawNode message;

		public GameLayer () : base (CCColor4B.Black)
		{
			DrawRightMenu ();
			InitBlockTypes ();
			InitGrid ();
			AddNewTetrisBlock ();
			Schedule (RunGameLogic, interval: this.gravity);
		}

		/// <summary>
		/// Draws the lateral menu.
		/// </summary>
		void DrawRightMenu(){
			
			CCDrawNode menu = new CCDrawNode ();
			this.AddChild (menu);

			menu.PositionX = 0;
			menu.PositionY = 0;

			var shape = new CCRect (Constants.SCREEN_WIDTH, 0, VisibleBoundsWorldspace.MaxX - Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			menu.DrawRect(shape,
				fillColor:CCColor4B.LightGray,
				borderWidth: 2,
				borderColor:CCColor4B.Gray);

			scoreValueLabel = new CCLabel ("0", "Arial", 50, CCLabelFormat.SystemFont);
			scoreValueLabel.PositionX = Constants.SCREEN_WIDTH + 20;
			scoreValueLabel.Color = CCColor3B.White;
			scoreValueLabel.PositionY = Constants.SCREEN_HEIGHT - 60;
			scoreValueLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
			this.AddChild (scoreValueLabel);

			CCLabel scoreLabel = new CCLabel ("Score:", "Arial", 50, CCLabelFormat.SystemFont);
			scoreLabel.PositionX = Constants.SCREEN_WIDTH + 20;
			scoreLabel.Color = CCColor3B.White;
			scoreLabel.PositionY = Constants.SCREEN_HEIGHT - 20;
			scoreLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
			this.AddChild (scoreLabel);

            linesValueLabel = new CCLabel("0", "Arial", 50, CCLabelFormat.SystemFont);
            linesValueLabel.PositionX = Constants.SCREEN_WIDTH + 20;
            linesValueLabel.Color = CCColor3B.White;
            linesValueLabel.PositionY = Constants.SCREEN_HEIGHT - 160;
            linesValueLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
            this.AddChild(linesValueLabel);

            CCLabel linesLabel = new CCLabel("Lines:", "Arial", 50, CCLabelFormat.SystemFont);
            linesLabel.PositionX = Constants.SCREEN_WIDTH + 20;
            linesLabel.Color = CCColor3B.White;
            linesLabel.PositionY = Constants.SCREEN_HEIGHT - 120;
            linesLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
            this.AddChild(linesLabel);

            levelValueLabel = new CCLabel("0", "Arial", 50, CCLabelFormat.SystemFont);
            levelValueLabel.PositionX = Constants.SCREEN_WIDTH + 20;
            levelValueLabel.Color = CCColor3B.White;
            levelValueLabel.PositionY = Constants.SCREEN_HEIGHT - 260;
            levelValueLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
            this.AddChild(levelValueLabel);

            CCLabel levelLabel = new CCLabel("Level:", "Arial", 50, CCLabelFormat.SystemFont);
            levelLabel.PositionX = Constants.SCREEN_WIDTH + 20;
            levelLabel.Color = CCColor3B.White;
            levelLabel.PositionY = Constants.SCREEN_HEIGHT - 220;
            levelLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
            this.AddChild(levelLabel);
        }

		/// <summary>
		/// Increments the score in the passed value.
		/// </summary>
		void IncrementScore(int increment){
			this.score += increment;
			scoreValueLabel.Text = this.score.ToString();
		}

        /// <summary>
        /// Add points for lines cleared.
        /// </summary>
        void ScoreLines(int numLines){
            int multiplier = 0;
            switch (numLines) {
                case 1:
                    multiplier = 40;
                    break;
                case 2:
                    multiplier = 100;
                    break;
                case 3:
                    multiplier = 300;
                    break;
                case 4:
                    multiplier = 1200;
                    break;
            }
            this.lines = this.lines + numLines;
            linesValueLabel.Text = this.lines.ToString();
            this.IncrementScore(multiplier * (level + 1));
            this.CalculateLevel();
        }

        /// <summary>
        /// Add points for blocks placed.
        /// </summary>
        void ScoreBlock(){
            this.IncrementScore(10 * (level + 1));
        }

        /// <summary>
        /// Calculate level (each 10 points is a level).
        /// </summary>
        void CalculateLevel(){
            int level = (this.lines / 10);
            if (level != this.level) {
                this.CalculateGravity();
                this.level = level;
            }
            levelValueLabel.Text = this.level.ToString();
        }

        /// <summary>
        /// Calculate gravity (each level pieces fall faster).
        /// </summary>
        void CalculateGravity(){
            float[] gravityValues = {
                0.8f, 0.717f, 0.633f, 0.55f, 0.467f, 0.383f, 0.3f, 0.217f, 0.133f, 0.1f,
                0.083f, 0.083f, 0.083f, 0.067f, 0.067f, 0.067f, 0.05f, 0.05f, 0.05f,
                0.033f, 0.033f, 0.033f, 0.033f, 0.033f, 0.033f, 0.033f, 0.033f, 0.033f, 0.033f };
            if (this.level <= gravityValues.Length) {
                this.gravity = gravityValues[this.level];
            } else {
                this.gravity = 0.017f;
            }
            Unschedule(RunGameLogic);
            Schedule(RunGameLogic, interval: this.gravity);
        }

        /// <summary>
        /// Clear score
        /// </summary>
        void ResetScore(){
			this.score = 0;
			scoreValueLabel.Text = this.score.ToString();
            this.CalculateLevel();
        }

		/// <summary>
		/// Defines the list with all kinds of TetrisBlocks.
		/// </summary>
		void InitBlockTypes(){
			this.blockTypes = new Type[]{ typeof(IBlock), typeof(JBlock), typeof(LBlock), typeof(OBlock), typeof(SBlock), typeof(TBlock), typeof(ZBlock) };
		}

		/// <summary>
		/// Inits the virtual grid that can check colision and remove completed rows.
		/// </summary>
		void InitGrid(){
			this.grid = new TetrisGrid ();
		}

		/// <summary>
		/// Moves down the current TetrisBlock until it is at screen bottom.
		/// </summary>
		/// <param name="frameTimeInSeconds">Frame time in seconds.</param>
		void RunGameLogic(float frameTimeInSeconds)
		{
            if (block.IsStoped()) {
                ImportChildBlocks();
                AddNewTetrisBlock();
                if (!this.messageShown) {
                    ScoreBlock();
                }
			} else {
				block.DownStep ();
			}
        }

		void HideEndMessage(){
			this.message.RemoveAllChildren ();
			this.message.RemoveFromParent ();
			this.messageShown = false;
		}

		void ShowEndMessage(){
			if (!this.messageShown) {
				this.messageShown = true;

				this.message = new CCDrawNode ();
				this.message.Position = new CCPoint (VisibleBoundsWorldspace.MaxX / 2 - 200, Constants.SCREEN_HEIGHT / 2 - 200);
				this.message.ContentSize = new CCSize (340, 240);

				var shape = new CCRect (0,0, 340, 240);
				this.message.DrawRect(shape,
					fillColor:CCColor4B.White,
					borderWidth: 2,
					borderColor:CCColor4B.Black);

				CCLabel messageLabel = new CCLabel ("Game Over\nScore: " + this.score, "Arial", 50, CCLabelFormat.SystemFont);
				messageLabel.PositionX = 50;
				messageLabel.Color = CCColor3B.Black;
				messageLabel.PositionY = 210;
				messageLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
				this.message.AddChild (messageLabel);

				CCSprite paddleSprite = new CCSprite ("playagain");
				paddleSprite.PositionX = 170;
				paddleSprite.PositionY = 70;
				paddleSprite.ContentSize = new CCSize(250, 60);
				messageLabel.AnchorPoint = CCPoint.AnchorUpperLeft;
				this.message.AddChild (paddleSprite);

				var touchListener = new CCEventListenerTouchAllAtOnce ();
				touchListener.OnTouchesEnded = OnRestartTouched;

				AddEventListener (touchListener, paddleSprite);


				this.AddChild (this.message);
			}
		}

		void RestartGame(){
			this.ResetScore ();
			this.grid.RemoveAllLines ();
			this.HideEndMessage ();
		}

		/// <summary>
		/// Imports the child blocks from TetrisBlock to the Game Layer.
		/// </summary>
		void ImportChildBlocks(){
			List<Block> newBlocks = new List<Block> ();

			foreach (Block subBlock in block.Blocks)
			{
				Block newBlock = new Block (subBlock.Color4B);

				newBlock.PositionX = subBlock.PositionX + block.PositionX;
				newBlock.PositionY = subBlock.PositionY + block.PositionY;

				newBlocks.Add (newBlock);
				this.AddChild (newBlock);

				this.grid.MarkBlockWithPosition (newBlock.PositionX, newBlock.PositionY, newBlock);
			}

            int LinesRemoved = 0;
			foreach (Block newBlock in newBlocks.OrderBy(p => p.Line)) {
				if (this.grid.RemoveCompletedLines (newBlock.Line)) {
                    LinesRemoved++;
				}else if(newBlock.IsOverScene){
					ShowEndMessage ();
				}
			}
            if (LinesRemoved > 0) {
                this.ScoreLines(LinesRemoved);
            }

		}

		/// <summary>
		/// Removes the current TetrisBlock and adds another one to the screen.
		/// </summary>
		void AddNewTetrisBlock(){
			if (block != null) {block.RemoveFromParent ();};

			Random rnd = new Random();
			int typeIndex = rnd.Next (0, this.blockTypes.Length);
			Type type = blockTypes [typeIndex];

			block = (TetrisBlock)Activator.CreateInstance(type, this.grid);

			block.PositionX = Constants.SCREEN_WIDTH / 2 - (Constants.SCREEN_WIDTH % 2);
			block.PositionY = Constants.SCREEN_HEIGHT;
			this.AddChild (block);
		}


		/// <summary>
		/// Attaches events for touch
		/// </summary>
		protected override void AddedToScene ()
		{
			base.AddedToScene ();

			// Use the bounds to layout the positioning of our drawable assets
			CCRect bounds = VisibleBoundsWorldspace;

			// Register for touch events
			var touchListener = new CCEventListenerTouchAllAtOnce ();
			touchListener.OnTouchesEnded = OnTouchesEnded;
			AddEventListener (touchListener, this);

			touchListener.OnTouchesMoved = HandleTouchesMoved;
			AddEventListener (touchListener, this);
		}

		void HandleTouchesMoved (System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
		{
			
		}

		/// <summary>
		/// Defines the behaviour of the touch events.
		/// </summary>
		/// <param name="touches">Touches.</param>
		/// <param name="touchEvent">Touch event.</param>
		void OnTouchesEnded (List<CCTouch> touches, CCEvent touchEvent)
		{
			if(!this.messageShown) touchEvent.StopPropogation ();

			if (touches.Count > 0) {
				var angle = touches [0].Delta.Angle;
				CCPoint point = touches [0].StartLocationOnScreen;
				float distance = touches [0].LocationOnScreen.DistanceSquared(ref point);
				if (Math.Abs(distance) <= 20.0f) {
					block.RotateRight ();
				} else {
					if (angle > -Math.PI / 4 && angle < Math.PI / 4) {
						block.LeftStep ();
					} else if (angle > -3*Math.PI / 4 && angle < -Math.PI / 4) {
						block.SpeedDown ();
					} else if (angle > 3*Math.PI / 4 || angle < -3*Math.PI / 4){
						block.RightStep ();
					}
				}

			}
		}

		/// <summary>
		/// Raises the restart touched event.
		/// </summary>
		/// <param name="touches">Touches.</param>
		/// <param name="touchEvent">Touch event.</param>
		void OnRestartTouched (List<CCTouch> touches, CCEvent touchEvent)
		{
			this.RestartGame ();
		}
	}
}
