﻿using System;
using CocosSharp;
using System.Collections.Generic;

namespace Tetris.Common
{
	public class JBlock : TetrisBlock
	{
		public JBlock(TetrisGrid grid) : base(grid) { }

		protected override int[,] State0{
			get { return new int[,]{{0,0}, {1,0}, {1,1}, {1,2}}; }
		}
		protected override int[,] State1{
			get { return new int[,]{{0,0}, {1,0}, {2,0}, {0,1}}; }
		}
		protected override int[,] State2{
			get { return new int[,]{{0,0}, {0,1}, {0,2}, {1,2}}; }
		}
		protected override int[,] State3{
			get { return new int[,]{{2,0}, {0,1}, {1,1}, {2,1}}; }
		}
		public override CCColor4B Color{
			get { return CCColor4B.Blue; }
		}

	}
}

