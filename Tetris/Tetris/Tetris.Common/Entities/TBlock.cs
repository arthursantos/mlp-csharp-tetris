﻿using System;
using CocosSharp;

namespace Tetris.Common
{
	public class TBlock : TetrisBlock
	{
		public TBlock(TetrisGrid grid) : base(grid) { }

		protected override int[,] State0{
			get { return new int[,]{{1,0}, {0,1}, {1,1}, {2,1}}; }
		}
		protected override int[,] State1{
			get { return new int[,]{{1,0}, {1,1}, {1,2}, {0,1}}; }
		}
		protected override int[,] State2{
			get { return new int[,]{{0,0}, {1,0}, {2,0}, {1,1}}; }
		}
		protected override int[,] State3{
			get { return new int[,]{{1,0}, {1,1}, {1,2}, {2,1}}; }
		}
		public override CCColor4B Color{
			get { return CCColor4B.Magenta; }
		}
	}
}

