﻿using System;

namespace Tetris.Common
{
	public class Constants
	{
		public const int SCREEN_HEIGHT = 1000;
		public const int SCREEN_WIDTH = 500;
		public const int BLOCK_SIZE = 50;
	}
}

