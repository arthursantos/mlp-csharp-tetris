﻿using System;
using CocosSharp;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Tetris.Common
{
	public class TetrisGrid
	{
		private bool[,] occupedPositions;
		private CCNode[,] blocks;

		public TetrisGrid ()
		{
			this.occupedPositions = new bool[(int)(Constants.SCREEN_HEIGHT/Constants.BLOCK_SIZE), (int)(Constants.SCREEN_WIDTH/Constants.BLOCK_SIZE)];
			this.blocks = new CCNode[(int)(Constants.SCREEN_HEIGHT/Constants.BLOCK_SIZE), (int)(Constants.SCREEN_WIDTH/Constants.BLOCK_SIZE)];
		}

		public void MarkBlockWithPosition(float positionX, float positionY, CCNode block){
			int col, lin;
			col = (int)(positionX / Constants.BLOCK_SIZE);
			lin = (int)(positionY / Constants.BLOCK_SIZE);

			if (col >= 0 && col <= this.occupedPositions.GetUpperBound (1) && lin >= 0 && lin <= this.occupedPositions.GetUpperBound (0)) {
				this.occupedPositions [lin, col] = true;
				this.blocks [lin, col] = block;
			} else {
				block.RemoveFromParent ();
			}
		}

		public bool EmptyPosition(float positionX, float positionY){
			int col, lin;
			col = (int)(positionX / Constants.BLOCK_SIZE);
			lin = (int)(positionY / Constants.BLOCK_SIZE);

			if (col >= 0 && col <= this.occupedPositions.GetUpperBound (1) && lin >= 0 && lin <= this.occupedPositions.GetUpperBound (0)) {
				return (this.occupedPositions [lin, col] == false);
			} else {
				return true;
			}
		}

		public void RemoveAllLines(){
			int numLin = this.occupedPositions.GetUpperBound (0);
			int numCols = this.occupedPositions.GetUpperBound (1);

			for (int lin = 0; lin <= numLin; lin++) {
				for (int col = 0; col <= numCols; col++) {
					if (this.blocks [lin, col] != null) {
						this.blocks [lin, col].RemoveAllChildren ();
						this.blocks [lin, col].RemoveFromParent ();
					}
					this.blocks [lin, col] = null;
					this.occupedPositions [lin, col] = false;
				}	
			}	
		}

		public bool RemoveCompletedLines(int completedLine){
			int numLin = this.occupedPositions.GetUpperBound (0);
			int numCols = this.occupedPositions.GetUpperBound (1);

			if (completedLine > numLin) {
				return false;
			}

			//check each block of current row
			bool lineCompleted = true;
			for (int col = 0; col <= numCols && lineCompleted; col++) {
				lineCompleted = lineCompleted && this.occupedPositions [completedLine, col];
			}	
			if (lineCompleted) {
				for (int lin = completedLine; lin <= numLin; lin++) {
					for (int col = 0; col <= numCols; col++) {
						if (lin == completedLine) {
							this.blocks [lin, col].RemoveAllChildren ();
							this.blocks [lin, col].RemoveFromParent ();
							this.blocks [lin, col] = null;
							this.occupedPositions [lin, col] = false;
						}

						if (lin < numLin) {
							this.occupedPositions [lin, col] = this.occupedPositions [lin + 1, col];
							this.blocks [lin, col] = this.blocks [lin + 1, col];
							if (this.blocks [lin, col] != null) {
								this.blocks [lin, col].PositionY -= Constants.BLOCK_SIZE;
							}
						}


					}	
				}	
				return true;
			} else {
				return false;
			}

		}
	}
}

