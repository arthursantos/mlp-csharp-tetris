﻿using System;
using CocosSharp;

namespace Tetris.Common
{
	public class Block : CCNode
	{
		private CCDrawNode drawNode;
		private CCColor4B color;

		public Block (CCColor4B color) : base()
		{
			this.color = color;

			this.drawNode = new CCDrawNode ();
			this.AddChild (drawNode);

			drawNode.PositionX = 0;
			drawNode.PositionY = 0;

			var shape = new CCRect (
				0, 0, Constants.BLOCK_SIZE, Constants.BLOCK_SIZE);
			drawNode.DrawRect(shape,
				fillColor:this.color,
				borderWidth: 2,
				borderColor:CCColor4B.White);
			
		}

		public void DownStep(){
			this.PositionY -= Constants.BLOCK_SIZE;
		}

		public CCColor4B Color4B{
			get { return this.color; }
		}

		public int Line{
			get{return (int)(this.PositionY / Constants.BLOCK_SIZE);}
		}

		public bool IsOverScene {
			get { return this.Line >= ((int)Constants.SCREEN_HEIGHT / Constants.BLOCK_SIZE);}
		}
	}
}

