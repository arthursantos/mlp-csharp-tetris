﻿using System;
using CocosSharp;

namespace Tetris.Common
{
	public class TetrisBlock : CCNode
	{
		private TetrisGrid grid;
		private int state = 0;
		private int downSpeed = 1;

		public TetrisBlock (TetrisGrid grid) : base()
		{
			this.grid = grid;

			this.LoadPositions (this.State0, 0);
		}

		public void RotateRight(){
			switch (this.state) {
			case 0:
				LoadPositions(this.State1, 1);
				break;
			case 1:
				LoadPositions(this.State2, 2);
				break;
			case 2:
				LoadPositions(this.State3, 3);
				break;
			case 3:
				LoadPositions(this.State0, 0);
				break;
			}
			if (CheckColisionWithOffset (0.0f, 0.0f)) {
				RotateLeft ();
			}
		}

		public void RotateLeft(){
			switch (this.state) {
			case 0:
				LoadPositions(this.State3, 3);
				break;
			case 1:
				LoadPositions(this.State0, 0);
				break;
			case 2:
				LoadPositions(this.State1, 1);
				break;
			case 3:
				LoadPositions(this.State2, 2);
				break;
			}
			if (CheckColisionWithOffset (0.0f, 0.0f)) {
				RotateRight ();
			}
		}


		protected virtual void LoadPositions(int[,] positions, int state){
			this.RemoveAllChildren ();
			this.state = state;

			for (int cont = 0, len=positions.GetUpperBound(0); cont <= len; cont++) {
				Block block = new Block (this.Color);
				block.PositionX = positions[cont, 0]*Constants.BLOCK_SIZE;
				block.PositionY = positions[cont, 1]*Constants.BLOCK_SIZE;
				this.AddChild (block);
			}
		}


		public void DownStep(){
			bool colision = false;

			int testSpeed = 1;
			while (testSpeed <= this.downSpeed && colision == false) {
				colision = colision || CheckColisionWithOffset (0.0f, -testSpeed * Constants.BLOCK_SIZE);
				testSpeed += 1;
			}
			if (colision) {
				this.downSpeed = testSpeed-2;
			}

				
			this.PositionY -= this.downSpeed * Constants.BLOCK_SIZE;
		}

		private bool CheckColisionWithOffset(float offsetX, float offsetY){
			float screenBottom = VisibleBoundsWorldspace.MinY;
			float screenLeft = VisibleBoundsWorldspace.MinX;
			float screenRight = Constants.SCREEN_WIDTH;
			float newY = this.BoundingBoxTransformedToParent.MinY + offsetY;
			float newX = this.BoundingBoxTransformedToParent.MinX + offsetX;
			foreach (CCNode child in this.Children) {
				if (newY + child.BoundingBoxTransformedToParent.MinY < screenBottom) {
					return true;
				}
				if (newX + child.BoundingBoxTransformedToParent.MinX < screenLeft) {
					return true;
				}
				if (newX + child.BoundingBoxTransformedToParent.MaxX > screenRight) {
					return true;
				}
				if(!this.grid.EmptyPosition(newX + child.BoundingBoxTransformedToParent.MinX, newY + child.BoundingBoxTransformedToParent.MinY)){
					return true;
				}
			}
			return false;
		}

		public void LeftStep(){
			if (!CheckColisionWithOffset (2*Constants.BLOCK_SIZE, 0.0f)) {
				this.PositionX += Constants.BLOCK_SIZE;
			}
		}

		public void RightStep(){
			if (!CheckColisionWithOffset (-Constants.BLOCK_SIZE, 0.0f)) {
				this.PositionX -= Constants.BLOCK_SIZE;
			}
		}

		public void SpeedDown(){
			this.downSpeed = 5;
		}

		public bool IsStoped(){
			return (this.downSpeed == 0);
		}

		public CCRawList<CCNode> Blocks{
			get{ return this.Children; }
		}

		protected virtual int[,] State0{
			get { return new int[,]{{0,0}}; }
		}
		protected virtual int[,] State1{
			get { return new int[,]{{0,0}}; }
		}
		protected virtual int[,] State2{
			get { return new int[,]{{0,0}}; }
		}
		protected virtual int[,] State3{
			get { return new int[,]{{0,0}}; }
		}
		public virtual CCColor4B Color{
			get { return CCColor4B.White; }
		}
	}
}

