﻿using System;
using CocosSharp;

namespace Tetris.Common
{
	public class IBlock : TetrisBlock
	{
		public IBlock(TetrisGrid grid) : base(grid) { }

		protected override int[,] State0{
			get { return new int[,]{{1,3},{1,2}, {1,1}, {1,0}}; }
		}
		protected override int[,] State1{
			get { return new int[,]{{0,1}, {1,1}, {2,1}, {3,1}}; }
		}
		protected override int[,] State2{
			get { return new int[,]{{1,3}, {1,2}, {1,1}, {1,0}}; }
		}
		protected override int[,] State3{
			get { return new int[,]{{0,1}, {1,1}, {2,1}, {3,1}}; }
		}
		public override CCColor4B Color{
			get { return CCColor4B.Aquamarine; }
		}
	}
}

